class Project < ApplicationRecord
  #validates :code,            presence: true
  validates :name,            presence: true
  validates :description,     presence: true
  validates :password,        presence: true, length: { minimum: 8 }, on: :create
  validates :owner,           presence: true
  validates :email,           presence: false

  has_secure_password
  has_one :article
  accepts_nested_attributes_for :article
end
