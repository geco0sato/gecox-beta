class Article < ApplicationRecord
  KIND_RECRUITMENT = 1
  KIND_NEWS        = 2

  validates :title,     presence: true
  validates :content,   presence: true
  validates :kind,      presence: true, on: :create
  validates :is_active, presence: false

  belongs_to :project
end
