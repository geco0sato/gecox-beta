class User::ProjectsController < User::ApplicationController

  before_action :set_active_menu
  before_action :set_project, only: [:show, :edit, :update]

  def index
    @projects = Project.order('projects.updated_at DESC')
  end

  def show
  end

  def new
    @project = Project.new
    @project.build_article
  end

  def edit
  end

  def create
    @project = Project.new(create_project_params)

    if @project.save
      redirect_to recruitments_path, notice: 'プロジェクトを作成しました'
    else
      render :new
    end
  end

  def update
    if ! @project.authenticate(params.require(:project).permit(:pass)['pass'])
          return redirect_to edit_project_path(@project), alert: 'パスワードが違います'
    end

    if @project.update(project_params)
      return redirect_to @project, notice: 'プロジェクトを編集しました'
    else
      render :edit
    end
  end

  private
    def set_project
      @project = Project.find(params[:id])
    end

    def project_params
      params.require(:project).permit(:name, :description, :owner, :email)
    end

    def create_project_params
      params.require(:project).permit(:name, :description, :owner, :email, :password, article_attributes: [:title, :content]).to_h.deep_merge(article_attributes: {kind: Article::KIND_RECRUITMENT, is_active: true})
    end

    def set_active_menu
      @activeMenu = 'projects'
    end

end
