class User::ArticlesController < User::ApplicationController
  before_action :set_article, only: [:show, :edit, :update]

  # GET /articles
  def index
    @articles = Article.includes(:project).references(:project).where(is_active: true).order('articles.updated_at DESC')
  end

  # GET /articles/1
  def show
  end

  # GET /offers/1/edit
  def edit
  end

  # PATCH/PUT /offers/1
  def update
    if @article.update(article_params)
      redirect_to @article, notice: 'Offer was successfully updated.'
    else
      render :edit
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.includes(:project).references(:project).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:offer).permit(:name)
    end
end
