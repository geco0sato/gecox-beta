class User::CommentsController < User::ApplicationController

  # POST /projects
  def create
    @comment = Comment.new(create_comment_params)

    if @comment.save
      redirect_to recruitment_path, notice: 'プロジェクトを作成しました'
      redirect_back(fallback_location: root_path)
    end
  end

  private

    def create_comment_params
      params.require(:comment).permit(:from, :content)
    end

end
