class User::RecruitmentsController < User::ApplicationController

  before_action :set_active_menu
  before_action :set_article, only: [:show, :edit, :update]

  def index
    @articles = Article.includes(:project).references(:project).where(is_active: true).order('articles.updated_at DESC')
  end

  def show
  end

  def edit
  end

  def update
    project = Project.find(@article.project.id)
    if ! project.authenticate(params.require(:project).permit(:pass)['pass'])
          return redirect_to edit_recruitment_path(@article), alert: 'パスワードが違います'
    end

    if @article.update(article_params)
      redirect_to recruitment_path(@article), notice: '記事を編集しました'
    else
      render :edit
    end
  end

  private

    def set_article
      @article = Article.includes(:project).references(:project).find(params[:id])
    end

    def article_params
      params.require(:article).permit(:title, :content, :is_active)
    end

    def set_active_menu
      @activeMenu = 'recruitments'
    end

end
