class CreateInitTables < ActiveRecord::Migration[5.2]
  def change

    create_table :projects do |t|
      t.string     :name,            :null => false
      t.text       :description,     :null => false
      t.string     :password_digest, :null => false
      t.string     :owner,           :null => false
      t.string     :email,           :null => false

      t.timestamps
    end

    create_table :articles do |t|
      t.references :project,      :null => false, foreign_key: true
      t.boolean    :is_active,    :null => false, default: false
      t.boolean    :kind,         :null => false
      t.string     :title,        :null => false 
      t.text       :content,      :null => false

      t.timestamps
    end

  end
end
